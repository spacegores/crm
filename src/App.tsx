import React from 'react';
import {BrowserRouter, Switch, Redirect, Route} from "react-router-dom";
import Home from "./Pages/Home/Home";
import SearchAddress from "./Pages/SearchAddress/SearchAddress";
import Tables from "./Pages/Tables/Tables";
import Calendar from "./Pages/Calendar/Calendar";
import Maps from "./Pages/Maps/Maps";

function App() {
  return (
    <div className="App">
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/address" component={SearchAddress}/>
                <Route exact path="/tables" component={Tables}/>
                <Route exact path="/calendar" component={Calendar}/>
                <Route exact path="/maps" component={Maps}/>
                <Redirect to="/"/>
            </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
