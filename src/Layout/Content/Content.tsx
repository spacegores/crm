import React, {FC} from "react";
import styles from "./Content.module.css";

const Content:FC = ({ children }) => {
  return <div className={styles.content}>{children}</div>;
};

export default Content;
