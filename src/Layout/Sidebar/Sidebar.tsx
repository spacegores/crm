import React, {FC} from 'react';
import styles from './Sidebar.module.css'

const Sidebar:FC = ({children}) => {
    return (
        <aside className={styles.sidebar}>
            {children}
        </aside>
    );
};

export default Sidebar;
