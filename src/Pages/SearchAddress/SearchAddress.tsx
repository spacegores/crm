import React, { useRef, useState} from 'react';
import Header from "../../Layout/Header/Header";
import Container from "../../components/Container/Container";
import styles from "./SearchAddress.module.css";
import Logo from "../../components/Logo/Logo";
import Account from "../../components/Account/Account";
import Main from "../../Layout/Main/Main";
import Sidebar from "../../Layout/Sidebar/Sidebar";
import Content from "../../Layout/Content/Content";
import Nabvar from "../../components/Navbar/Navbar";
import AddressList from "../../components/AdressList/AddressList";
import Button from "../../components/Button/Button";
import SearchIcon from "../../components/Icons/SearchIcon";


interface refData {
    current: HTMLInputElement | null
}

const SearchAddress = () => {

    const [address, setAddress] = useState([]);
    const [error, setError] = useState('');

    const inputRef:refData = useRef(null);

    const getData = async () => {

        setAddress([])

        setError('')

        if(inputRef.current && inputRef.current.value.length <= 3) {
            setError('Ошибка! Введите больше 3х символов в поисковую строку')
            return;
        }

        const url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
        const token = "d575d97e106eabf9ac3ba89606640cff33c13411";
        let query = inputRef?.current?.value;

        const options = {
            method: "POST",
            mode: 'cors' as RequestMode,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Token " + token
            },
            body: JSON.stringify({query: query})
        }

        await fetch(url, options)
            .then(response => response.json())
            .then(result => setAddress(result.suggestions))
            .catch(error => console.log("error", error));
    }

    return (
        <>
            <Header>
                <Container className={'header-container'}>
                    <Logo width={48} height={48}/>
                    <Account width={48} height={48} accName={'Имя Фамилия'}/>
                </Container>
            </Header>
            <Main>
                <Container className={'main-container'}>
                    <Sidebar>
                        <Nabvar/>
                    </Sidebar>
                    <Content>
                        <h1 className={styles.title}>Поиск адресов</h1>
                        <p className={styles.subtitle}>Введите интересующий вас адрес</p>
                        <div className={styles.wrapper}>
                            <input className={error ? styles.error : styles.search} type="text" ref={inputRef} placeholder={'Введите интересующий вас адрес'}/>
                            <Button icon={<SearchIcon height={20} width={20} fill={'#fff'}/>} onClick={getData}>Поиск</Button>
                        </div>
                        {
                            address?.length > 0 && <AddressList address={address}/>
                        }

                        {error && error}

                    </Content>
                </Container>
            </Main>
        </>
    );
};

export default SearchAddress;
