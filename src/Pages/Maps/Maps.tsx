import React, {FC} from 'react';
import Header from "../../Layout/Header/Header";
import Container from "../../components/Container/Container";
import Logo from "../../components/Logo/Logo";
import Account from "../../components/Account/Account";
import Main from "../../Layout/Main/Main";
import Sidebar from "../../Layout/Sidebar/Sidebar";
import Content from "../../Layout/Content/Content";
import Nabvar from "../../components/Navbar/Navbar";

const Maps:FC = () => {
    return (
        <>
            <Header>
                <Container className={'header-container'}>
                    <Logo width={48} height={48}/>
                    <Account width={48} height={48} accName={'Имя Фамилия'}/>
                </Container>
            </Header>
            <Main>
                <Container className={'main-container'}>
                    <Sidebar>
                        <Nabvar/>
                    </Sidebar>
                    <Content>
                        <h1>Карты</h1>
                    </Content>
                </Container>
            </Main>
        </>
    );
};

export default Maps;
