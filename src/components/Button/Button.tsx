import React, {FC} from 'react';
import styles from './Button.module.css'

interface ButtonProps {
    icon: React.ReactElement
    onClick: () => void
}

const Button:FC<ButtonProps> = ({children, icon, onClick}) => {
    return (
        <button onClick={() => onClick()} className={styles.button}>
            {icon && icon}
            {children}
        </button>
    );
};

export default Button;
