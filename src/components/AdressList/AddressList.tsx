import React, {FC} from 'react';
import styles from './AddressList.module.css'

interface IAdress {
    value: string
}

interface AddressListProps {
    address: IAdress[]
}

const AddressList: FC<AddressListProps> = ({address}) => {


    return (
        <div className={styles.wrapper}>
            <h2 className={styles.title}>Адреса</h2>
            <ul className={styles.list}>
                {
                    address?.map(val => {
                        return <li key={Date.now() + val.value} className={styles.item}>{val.value}</li>
                    })
                }
            </ul>
        </div>
    );
};

export default AddressList;
