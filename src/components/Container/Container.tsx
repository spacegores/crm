import React, {FC} from 'react';
import styles from './Container.module.css'
import clsx from 'clsx'

interface ContainerI {
    className: string
}

const Container:FC<ContainerI> = ({children, className}) => {
    return (
        <div className={clsx(styles.container, className)}>
            {children}
        </div>
    );
};

export default Container;
