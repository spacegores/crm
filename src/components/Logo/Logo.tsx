import React, {FC} from 'react';
import styles from './Logo.module.css'
import {Link} from "react-router-dom";

interface LogoI {
    width: number
    height: number
}

const Logo:FC<LogoI> = ({width, height}) => {
    return (
        <Link to={'/'} className={styles.logo}>
            <img width={width} height={height} src={'./logoIcon.svg'} alt="Логотип"/>
            <span>Wrench CRM</span>
        </Link>
    );
};

export default Logo;
