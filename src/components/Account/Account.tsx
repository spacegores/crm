import React, { FC } from "react";
import styles from "./Account.module.css";

interface AccountProps {
  width: number;
  height: number;
  accName: string;
}

const Account: FC<AccountProps> = ({ width, height, accName }) => {
  return (
    <div className={styles.account}>
      <img
        className={styles.image}
        width={width}
        height={height}
        src="./icPerson.svg"
        alt="Аккаунт"
      />
      <span>{accName}</span>
    </div>
  );
};

export default Account;
