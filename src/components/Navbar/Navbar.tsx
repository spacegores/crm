import React, {FC, useState} from 'react';
import styles from './Navbar.module.css'
import {Link} from "react-router-dom";
import SearchIcon from "../Icons/SearchIcon";
import HomeIcon from "../Icons/HomeIcon";
import NavArrowIcon from "../Icons/NavArrowIcon";
import TablesIcon from "../Icons/TablesIcon";
import CalendarIcon from "../Icons/CalendarIcon";
import MapIcon from "../Icons/MapIcon";
import WidgetIcon from "../Icons/WidgetIcon";
import OptionIcon from "../Icons/OptionIcon";
import LogoutIcon from "../Icons/LogoutIcon";
import ProfileIcon from "../Icons/ProfileIcon";
import FinanceIcon from "../Icons/FinanceIcon";



const Navbar:FC = () => {

    const [showSub, setShowSub] = useState(false);

    return (
       <nav className={styles.nav}>
           <h2 className={styles.title}>Меню</h2>
           <ul>
               <li>
                   <Link to={'/'}>
                       <HomeIcon width={32} height={32}/>
                       <span>Главная</span>
                   </Link>
               </li>
               <li>
                   <Link to={'/address'}>
                       <SearchIcon width={32} height={32}/>
                       <span>Поиск адресов</span>
                   </Link>
               </li>
               <li>
                   <Link to={'/tables'}>
                       <TablesIcon width={32} height={32}/>
                       <span>Таблицы</span>
                   </Link>
               </li>
               <li>
                   <Link to={'/calendar'}>
                       <CalendarIcon width={32} height={32}/>
                       <span>Календарь</span>
                   </Link>
               </li>
               <li>
                   <Link to={'/maps'}>
                       <MapIcon width={32} height={32}/>
                       <span>Карты</span>
                   </Link>
               </li>
               <li>
                   <Link to={'/widgets'}>
                       <WidgetIcon width={32} height={32}/>
                       <span>Виджеты</span>
                   </Link>
               </li>
               <li>
                   <div onClick={() => setShowSub(!showSub)} className={styles.child}>
                       <span className={!showSub ? styles.subIcon : styles.subIconActive}><NavArrowIcon width={15} height={15}/></span>
                       <OptionIcon width={32} height={32}/>
                       <span>Настройки</span>
                   </div>
                   <ul className={!showSub ? styles.submenu : styles.active}>
                       <li>
                           <Link to={'/profile-settings'}>
                               <ProfileIcon width={32} height={32}/>
                               <span>Настройки профиля</span>
                           </Link>
                       </li>
                       <li>
                           <Link to={'/profile-settings'}>
                               <FinanceIcon width={32} height={32}/>
                               <span>Управление финансами</span>
                           </Link>
                       </li>
                   </ul>
               </li>
               <li>
                   <Link to={'/logout'}>
                       <LogoutIcon width={32} height={32}/>
                       <span>Выход</span>
                   </Link>
               </li>
           </ul>
       </nav>
    );
};

export default Navbar;
